{
	"mapcontainer":"Map2",
	"mapwidgets":{
		"3D Kartenansicht":{
			"type":"vcs.vcm.maps.Agency9",
			"startingmap":true, 
			"epsg":	"25833",			
			"codebase":"../../lib/thirdparty/3dmaps/",			
			"Layers":{	
				"orthophoto":{
					"id":"agency9_orthophoto",
					"name":"Ortophoto",
					"type":"vcs.vcm.layer.agency9.BaseLayer",
					"modelurl":"http://hosting.virtualcitysystems.de/demos/berlin/vcm-api/data/baselayer/",
					"visibility":true
				},
				"agency9_buildings":{
					"id":"agency9_buildings",
					"name":"3D-Geb??ude",
					"type":"vcs.vcm.layer.agency9.ModelLayer", 
					"modelurl":"http://hosting.virtualcitysystems.de/demos/berlin/vcm-api/data/untexturedbuildings/",					             
					"visibility":true									
				}
			}	
		}
	}
}