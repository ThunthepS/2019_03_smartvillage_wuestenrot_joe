precision highp float;
uniform vec4 u_diffuse;
varying vec3 v_normal;
varying float v_specularIntensity;
void main(void) {
  float diffuseIntensity = max(dot(v_normal,czm_sunDirectionEC), 0.) * 0.9;
  vec4 diffuseColor = u_diffuse;
  vec3 lightColor = vec3(1.0, 1.0, 1.0);
  vec4 color = vec4(diffuseColor.rgb * 0.5 + diffuseColor.rgb * diffuseIntensity + lightColor.rgb * v_specularIntensity, diffuseColor.a);
  gl_FragColor = color;
}
