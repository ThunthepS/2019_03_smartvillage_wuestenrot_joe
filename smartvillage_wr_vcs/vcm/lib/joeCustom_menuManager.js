var menu_building_info = function () {
  if (info_building.style.display === 'none') {
      $("#info_building").show("1000");
      $("#buildinginfo_icon_up").show();
      $("#buildinginfo_icon_down").hide();
  } else {
      $("#info_building").hide("1000");
      $("#buildinginfo_icon_down").show();
      $("#buildinginfo_icon_up").hide();
  }
}


var show_info = function (){
  $("#chart").hide("1000");
  $("#info_building").show("2000");
}