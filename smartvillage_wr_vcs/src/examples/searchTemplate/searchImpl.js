/* global UnderscoreTemplate axios vcs */
import defaultOptions from './config.json';

export default class SearchImpl {
  /**
   * Initialize array for search results, adds resultLayer to framework,
   * adds balloonInfo to resultLayer, adds property-values from options object or from default values.
   * @constructor
   * @param {!Object} options - All properties are optional, uses default values if not specified
   * @param {Object=} options.layerOptions - Options for vcs.vcm.layer.Vector object used for displaying balloon-info
   * @param {(Array<string>|string)=} options.balloonInfo - Structure of balloon-info. Underscore-Template syntax
   * @param {(Array<string>|string)=} options.title - Headline of the search result. Underscore-Template syntax allowed
   * @param {(Array<string>|string)=} options.description - Description of the search result. Underscore-Template syntax allowed
   * @param {string=} options.url - URL of the search service
   * @param {Object=} options.defaultQueryParams - default nominatim params to be added to every query such as extent or countrycode
   * @param {Array<string>=} options.queryAdditions - strings to be added to the query, such as city or state.
   */
  constructor(options) {
    function createTemplate(template) {
      const string = Array.isArray(template) ? template.join('') : template;
      return UnderscoreTemplate(string);
    }

    const config = Object.assign(defaultOptions, options);
    const resultLayer = new vcs.vcm.layer.Vector(config.layerOptions);
    this.resultLayerName = resultLayer.getName();
    this.balloonInfo = config.balloonInfo;
    this.title = createTemplate(config.title);
    this.description = createTemplate(config.description);
    this.url = config.url;
    this.defaultQueryParams = config.defaultQueryParams;
    this.queryAdditions = config.queryAdditions;
    this._currentResults = [];

    const framework = vcs.vcm.Framework.getInstance();
    framework.addLayer(resultLayer);
    const balloonInfo = framework.getWidgetByType('vcs.vcm.widgets.BalloonInfo');
    if (balloonInfo && !balloonInfo.hasTypeForLayerName(this.resultLayerName)) {
      balloonInfo.addTypeForLayer(this.resultLayerName, this.balloonInfo, 'balloonHeight + 30', '70');
    }
  }

  /**
   * Sending search request and store data response.
   * @param {string} query - the trimmed user string input
   * @return {Promise}
   */
  search(query) {
    this.clear();
    const params = Object.assign(
      {
        q: [query].concat(this.queryAdditions).join(', '),
        format: 'json',
        polygon_geojson: 1,
        addressdetails: 1,
      },
      this.defaultQueryParams,
    );

    return axios.get(this.url, { params })
      .then((response) => {
        this._currentResults.push(...response.data.map((obj) => {
          const option = {
            pointWGS84: [Number(obj.lon), Number(obj.lat)],
            result: obj,
            layerName: this.resultLayerName,
            title: this.title(obj),
            description: this.description(obj),
          };
          if (obj.geojson) {
            option.geom = obj.geojson;
          }

          return new vcs.vcm.widgets.search.FeatureItem(option);
        }));
        return Promise.resolve();
      })
      .catch((error) => {
        console.error(error);
      });
  }

  /**
   * Clears the search
   */
  clear() {
    this._currentResults.splice(0);
    const resultLayer = vcs.vcm.Framework.getInstance().getLayerByName(this.resultLayerName);
    resultLayer.activate(false);
  }

  /**
   * @return {Array}
   */
  get currentResults() {
    return this._currentResults;
  }
}

