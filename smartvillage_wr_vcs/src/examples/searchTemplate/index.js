import Search from './searchImpl';

const framework = vcs.vcm.Framework.getInstance();
function register(){
  vcs.ui.registerPlugin({
    name: 'customSearch',
    search: {
      search: new Search(framework.getConfig('ui.plugins.customSearch')),
    },
  });
}

if (framework.isInitialized()) {
  register();
} else {
  framework.subscribe('FRAMEWORK_INITIALIZED', register);
}
