import headerButton1 from './headerButton1';
import headerButton2 from './headerButton2';
import headerButton3 from './headerButton3';

vcs.ui.registerPlugin({
  name: 'buttons',
  widgetButton: [ // you can add multiple buttons as an array
    '<LeadButton @click="runExample">T2</LeadButton>', // buttons can be templates using global vue components
    '<span onclick="runExample">T3</span>', // buttons can be standard HTML and execute global functions
  ],
  toolboxButton: '<IframeLink iframe="http://www.virtualcitysystems.de">VCS</IframeLink>',
  headerButton: [headerButton1, headerButton2, headerButton3], // buttons can be vue components
  mapButton: '<button>MAP</button>',
});

window.runExample = function() {
  alert('running example');
};
