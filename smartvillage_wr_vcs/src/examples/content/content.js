import example from './example';

const routes = [{
  name: 'example',
  path: '/pluginExample',
  component: example,
}];

const store = {
  state: {
    ticker: 0,
  },
  mutations: {
    setTicker(state, ticker) {
      state.ticker = ticker;
    },
  },
};

vcs.ui.registerPlugin({
  name: 'content',
  routes,
  store,
  widgetButton: '<a href="#/pluginExample"></a>',
});
