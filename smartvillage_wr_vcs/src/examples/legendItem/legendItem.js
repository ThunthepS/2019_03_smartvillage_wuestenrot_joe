import component from './legendItemComponent';
import mapComponent from './mapComponent';

const store = {
  state: {
    on: false,
  },
  mutations: {
    toggle(state) {
      state.on = !state.on;
    },
  },
};

vcs.ui.registerPlugin({
  name: 'legendItem',
  store,
  legendItem: {
    name: 'item',
    component,
  },
  mapComponent,
});
