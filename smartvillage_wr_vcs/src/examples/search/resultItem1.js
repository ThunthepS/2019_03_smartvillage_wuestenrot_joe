export default class {
  constructor(name) {
    this.name = name;
  }

  click() {
    alert(`You clicked ${this.name}`);
  }
}
