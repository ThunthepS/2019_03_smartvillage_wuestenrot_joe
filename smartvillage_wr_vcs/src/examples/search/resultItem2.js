export default class {
  constructor(name) {
    this.name = name;
  }

  click() {
    console.log(`Context Location: ${this.name}`);
  }
}
