import button from './button';
import component from './component';
import resultItem1 from './resultItem1';
import resultItem2 from './resultItem2';
import resultItem1Component from './resultItem1Component';
import resultItem2Component from './resultItem2Component';
import search from './searchImpl';

vcs.ui.registerPlugin({
  name: 'searchPlugin',
  locationBasedPlugin: {
    searchComponent: {
      name: 'example',
      component,
    },
    searchButton: button,
  },
  search: {
    search,
    resultItem: [
      {
        resultItemConstructor: resultItem1,
        component: resultItem1Component,
      },
      {
        resultItemConstructor: resultItem2,
        component: resultItem2Component,
      },
    ],
  },
});
