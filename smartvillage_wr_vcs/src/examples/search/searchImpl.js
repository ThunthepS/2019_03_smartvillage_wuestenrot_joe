import Result1 from './resultItem1';
import Result2 from './resultItem2';

class SearchImpl {
  constructor() {
    this._currentResults = [];
  }

  search(query) {
    this._currentResults.push(new Result1(query));
    return Promise.resolve();
  }

  contextLocation(location) {
    this._currentResults.push(new Result2(`lon: ${location.longitude}, lat: ${location.latitude}`));
  }

  clear() {
    this._currentResults.splice(0);
  }

  autocomplete() {
    return Promise.resolve(['test']);
  }

  get currentResults() {
    return this._currentResults;
  }
}
const search = new SearchImpl();
export default search;
