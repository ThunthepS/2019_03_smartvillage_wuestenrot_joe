var gulp = require('gulp');
var gutil = require('gulp-util');
var replace = require('gulp-replace');
var gulpif = require('gulp-if');
var rename = require('gulp-rename');
var zip = require('gulp-zip');
var del = require('del');
var vinylPaths = require('vinyl-paths');
var runSequence = require('run-sequence');
var program = require('commander');

var webpack = require('webpack');
var webpackConfig = require('./build/webpack.prod.conf');

program
  .option('-p, --plugin <name>', 'the name to use for the plugin, overwrites the default')
  .option('-l, --library <name>', 'to export the plugin as a library')
  .parse(process.argv);
var pluginName = program.plugin;

gulp.task('clean', function() {
  return del([
    'dist/*'
  ]);
});

gulp.task('webpack', function(cb) {
  if (program.library) {
    webpackConfig.output.library = program.library;
    webpackConfig.output.libraryTarget = 'umd';
  }
  webpack(webpackConfig, function(err) {
    if (err) {
      throw new gutil.PluginError('webpack', err);
    }
    gutil.log('[webpack]', 'webpack compiled');
    cb();
  });
});

gulp.task('replace', function() {
  return gulp.src(['dist/*.js'])
    .pipe(gulpif(!pluginName, replace(/registerPlugin\((.*)\)/, function(match) {
      pluginName = match.match(/name:"(\w+)"/)[1];
      return match;
    })))
    .pipe(replace(/\.?\/?assets\//g, function() {
      return 'plugins/' + pluginName + '/assets/';
    }))
    .pipe(vinylPaths(del))
    .pipe(rename(function(path) {
      path.basename = pluginName;
    }))
    .pipe(gulp.dest('dist/'));
});

gulp.task('zip', function() {
  return gulp.src(['dist/*.js', 'assets/**/*', 'config.json'], { base: process.cwd() })
    .pipe(rename(function (path) {
      var ext = path.dirname !== 'dist' && path.dirname !== '.' ? path.dirname : '';
      path.dirname = pluginName + '/' + ext;
    }))
    .pipe(zip(pluginName + '.zip'))
    .pipe(gulp.dest('dist'));
});

gulp.task('build', function(cb) {
  runSequence(
    'clean',
    'webpack',
    'replace',
    'zip',
    cb
  );
});

gulp.task('default', ['build']);
