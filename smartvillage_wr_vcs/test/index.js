import Vue from 'vue';

Vue.config.productionTip = false;

// Polyfill fn.bind() for PhantomJS
/* eslint-disable no-extend-native */
Function.prototype.bind = require('function-bind');

// require all test files (files that ends with .spec.js)
const testContext = require.context('./', true, /\.spec$/);
testContext.keys().forEach(testContext);
