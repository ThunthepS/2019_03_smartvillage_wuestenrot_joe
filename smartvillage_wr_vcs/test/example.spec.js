import Vue from 'vue';
import example from '../src/examples/buttons/headerButton2';

describe('Example Test Suite', () => {
  const sandbox = sinon.sandbox.create();
  afterEach(() => { sandbox.restore(); });

  it('toggle should change the button class', () => {
    const vm = new Vue({
      template: '<div><example></example></div>',
      components: { example }
    }).$mount();
    const button = vm.$el.querySelector('button');
    button.click();
    expect(vm.$children[0].$data).to.have.property('on', true);
  });

  it('should allow spying on a function', () => {
    const spy = sandbox.spy(example.methods, 'toggle');
    const vm = new Vue({
      template: '<div><example></example></div>',
      components: { example }
    }).$mount();
    const button = vm.$el.querySelector('button');
    button.click();
    expect(spy).to.have.been.calledOnce;
  });

  it('should allow the stubbing out of a function', () => {
    const stub = sandbox.stub(example.methods, 'toggle');
    const vm = new Vue({
      template: '<div><example></example></div>',
      components: { example }
    }).$mount();
    const button = vm.$el.querySelector('button');
    button.click();
    expect(stub).to.have.been.calledOnce;
    expect(vm.$children[0].$data).to.have.property('on', false);
  });
});
