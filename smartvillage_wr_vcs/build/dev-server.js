// var config = require('../config')
process.env.NODE_ENV = "development";

var opn = require('opn');
var express = require('express');
var webpack = require('webpack');
var webpackConfig = require('./webpack.dev.conf');

var examples = {
  buttons: './src/examples/buttons/buttons.js',
  content: './src/examples/content/content.js',
  search: './src/examples/search/search.js'
};

if (process.argv.length === 3) {
  var argEx = process.argv[2];
  if (argEx === 'examples') {
    for (var ex in examples) {
      webpackConfig.entry.app.push(examples[ex]);
    }
  } else if (examples[argEx]) {
    webpackConfig.entry.app.push(examples[argEx]);
  }
}

var port = 8081;
var autoOpenBrowser = true;

var app = express();
var compiler = webpack(webpackConfig);

var devMiddleware = require('webpack-dev-middleware')(compiler, {
  publicPath: webpackConfig.output.publicPath,
  quiet: true
});

var hotMiddleware = require('webpack-hot-middleware')(compiler, {
  log: () => {}
});
// force page reload when html-webpack-plugin template changes
compiler.plugin('compilation', function (compilation) {
  compilation.plugin('html-webpack-plugin-after-emit', function (data, cb) {
    hotMiddleware.publish({ action: 'reload' });
    cb();
  });
});

app.use(require('connect-history-api-fallback')());
app.use(devMiddleware);
app.use(hotMiddleware);

// serve pure static assets
app.use("/assets", express.static('./assets'));
app.use("/lib", express.static('./vcm/lib'));
app.use("/config.json", express.static('./vcm/config.json'));
app.use("/css", express.static('./vcm/css'));
app.use("/fonts", express.static('./vcm/fonts'));
app.use("/images", express.static('./vcm/images'));
app.use("/img", express.static('./vcm/img'));
app.use("/templates", express.static('./vcm/templates'));
app.use("/examples", express.static('./vcm/examples'));
app.use("/doc", express.static('./vcm/doc'));

var uri = 'http://localhost:' + port;

devMiddleware.waitUntilValid(function () {
  console.log('> Listening at ' + uri + '\n');
});

module.exports = app.listen(port, function (err) {
  if (err) {
    console.log(err);
    return;
  }

  // when env is testing, don't need open it
  if (autoOpenBrowser && process.env.NODE_ENV !== 'testing') {
    opn(uri);
  }
});
