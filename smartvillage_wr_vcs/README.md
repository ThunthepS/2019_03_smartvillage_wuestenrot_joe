#### Getting Started
Requirements: **NodeJS 6 LTS** and **npm 3** in your PATH.
1. Check the folder structure. An exported (compiled) **virtualcityMAP** must be in the _vcm_ path, relative to the root of your project
```
- .
  |- build/
  |- src/
  |- test/
  |- vcm/ <- needs to be copied
  |- .babelrc
  |- package.json
```
2. Install all the needed dependencies using `npm install`
3. To run the examples, execute `npm run examples`

#### Reading the _docs_
Once you habe the development server running, you will find the documentation of the **vcm** API
at [http://localhost:8081/doc/index.html](http://localhost:8081/doc/index.html).

Further reading:
- [Vuejs](https://vuejs.org/)
- [vue-router](https://router.vuejs.org/en/)
- [vuex](https://vuex.vuejs.org/en/)
- [openlayers](https://openlayers.org/en/latest/apidoc/index.html)
- [Cesium](https://cesiumjs.org/Cesium/Build/Documentation/index.html)

### Writing your own plugin
Using the examples as a basis, you can start writing your own plugins. Make sure to register your plugin by calling `vcs.ui.registerPlugin(options)`.
To start writing your own plugin, simple use the `src/index.js` as an entry point for your own plugin.
To stop running the examples, and run you own plugin, start the dev server with `npm start`.

It is important to only write _a single plugin_ and only call _vcs.ui.registerPlugin_ **once**.
Otherwise the build process will not properly run through and the
configuration with the **virtualcityPUBLISHER** becomes impossible.

Once you are satisfied with your plugin, you can build it using `npm run build`.
This will create a zip-file containing your applications **bundled code** and **assets**.

To add your plugin to the **virtualcityPUBLISHER**, unzip the contents of this file into 
the `root/public/plugins/` folder. The structur of the `public/plugins/` folder must look like this:
```
public/plugins
  |- myPlugin/
    |- assets/
    |- myPlugin.js
    |- config.json
  |- mySecondPlugin/
    |- assets/
    |- mySecondPlugin.js
  |- noAssetsPlugin/
    |- noAssetsPlugin.js
    |- config.json
```
Your plugin can now be added to **virtualcityMAP**s.

To add your plugin to a deployed map, simply unzip the distribution into a folder
`plugins/`. 
Add your plugin to a deployed map, by adding your plugin to the `config.json` under _ui.plugins_ 
and providing the name of your served plugin as 
the key (this is especially useful if you wish to pass configurations to your plugin):
> This only works properly of you maintain the folder structure provided by the build script
> and use the `plugins/` directory on your server.
> If you use the virtualcityPUBLISHER to create your maps, this should not be an issue.
```json
  ...,
  "ui": {
    "plugins": {
      "myPlugin": {
        ...
      }
    }    
  }
```
#### Adding static data to your plugin
You may wish to add your own icons, images or other forms of static content. For this, use the 
`assets/` folder. You can then request your resource with `assets/yourResource`. The build script
will automatically change this URLs to `plugins/myPlugin/assets/yourResource` for ease of deployment

#### Adding configuration to your plugin
Some plugins might need configuring. You can add your configuration to the vcm's `config.json`
in the section _ui.plugins.myPlugin_ as shown above. To allow for a configuration of your
plugin by the **virtualcityPUBLISHER**, you can specify the configurable parameters of your
plugin in the `config.json` found in the root of your application. Add the keys
and their default values to this JSON.
```json
{
  "myKey": "defaultValue",
  "myNumeric": 123,
  "myBoolean": true,
  "myArray": [1, 2, 3]
}
```

#### Testing your plugin
Test are written for the `mocha` test framework, using `chai` for assertation and `sinon` for spies, stubs and mocks and `karma` as a test runner.
To run the tests, run `npm test`. In the `test/` directory you will find a small example _spec_.
